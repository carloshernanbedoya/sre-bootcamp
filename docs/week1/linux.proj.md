# Linux CLI

!!! info inline end
    20 ~ 35 mins

Create a running container where we will run our linux project.


```bash
cd ~
docker run -it -v $(pwd)/linux:/tmp/ ubuntu:latest bash
```



- [ ] Create a folder called `linux` in your home directory
- [ ] Install any text editor (vim, nano, vi)
---
- [ ] Create a file called `hello.sh` in the `linux` folder
- [ ] Add the following content to the `hello.sh` file:

```bash
#!/bin/bash
echo "Hello World"

```

- [ ] Make the file executable
- [ ] Run the file
- [ ] Edit the file to receieve a parameter and print it instead of World (example: ./hello.sh John! => Hello John!)
- [ ] Edit the file to substitute an environment variable `${NAME}` and print it (Example NAME=Glen ./hello.sh => Hello Glen)

---

- [ ] Display how much time the system has been up
- [ ] Display the current user
- [ ] Display the current user's id
- [ ] Use top to display the processes running.
